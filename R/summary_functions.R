# summary functions

svy_table_ = function(`data`,args){
    wv = noquote(get_weight_var(`data`))
    names = as.character(unlist(lapply(args,function(x) x$expr)))
    # weight variable set
    if (!is.null(wv)){
        # Check weight variable in data passed
        if(any(grepl(wv,names(`data`)))) {
            # Extract the table for the events
            args2=args
            args2[[length(args)+1]] = lazy(noquote(eval(parse(text=wv))))
        } else {
            stop('Weight variable not in data')
        }
    } 
    dta = as.data.frame(lazy_eval(args2,data=`data`))
    names(dta) = c(names,"weight")
    if (is.weighted(`data`)) {
        return(dta %>% group_by_(.dots=lapply(names,as.symbol)) %>% summarise(Count=sum(weight)))
    } else {
        return(dta %>% group_by_(.dots=lapply(names,as.symbol)) %>% summarise(Count=n()))
    }
}

#' svy_table - calculate weighted table
#'
#' @param data the dataset to calculate on
#' @param ... list of variables to group by
#'
#' @return
#' @export
#'
#' @examples
svy_table = function(`data`,...){
    svy_table_(`data`,lazy_dots(...))
}


svy_means_ = function(`data`,args){
    wv = get_weight_var(`data`)
    names = as.character(unlist(lapply(args,function(x) x$expr)))
    # weight variable set
    if (!is.null(wv)){
        # Check weight variable in data passed
        if(any(grepl(wv,names(`data`)))) {
            # Extract the table for the events
            args2=args
            args2[[length(args)+1]] = lazy(noquote(eval(parse(text=wv))))
        } else {
            stop('Weight variable not in data')
        }
    } 
    if (is.weighted(`data`)) {
        return(eval(parse(text=paste0("data %>% summarise(wtd.mean(",names,",weights=",wv,"))"))))
    } else {
        return(eval(parse(text=paste0("data %>% summarise(mean(",names,"))"))))
    }
}

#' svy_means - calculate weighted means
#'
#' @param data the dataset to calculate on
#' @param ... list of variables to group by
#'
#' @return
#' @export
#'
#' @examples
svy_means = function(`data`,...){
    x=`data`
    svy_means_(`data`,lazy_dots(...))
}